//
// Created by damian on 11.03.21.
//

#ifndef WAVETEST_MPPCFIT_H
#define WAVETEST_MPPCFIT_H


#include "TMath.h"


unsigned gFitMaxNpe = 10;


// https://www.caen.it/download/?filter=DT5702
double igorFit(const double *x, const double *par) {
    const double norm = par[0];
    const double gain = par[1];
    const double zero = par[2];
    const double noise = par[3];
    const double avnpe = par[4];
    const double excess = par[5];
    const double xtalk = par[6];

    double y = 0.;
    double lastPeakInt;
    for (unsigned p = 0; p <= gFitMaxNpe; ++p) {
        const double peak = zero + gain * p;
        double peakInt = TMath::Poisson(p, avnpe);
        if (p > 1) {
            peakInt += lastPeakInt * xtalk;
        }
        lastPeakInt = peakInt;
        y += peakInt * TMath::Gaus(x[0], peak, TMath::Sqrt(noise * noise + p * excess * excess), true);
    }
    y *= norm;

    return y;
}


// https://zeus.phys.uconn.edu/wiki/index.php/Characterizing_SiPMs
double zeusFit(const double *x, const double *par) {
    const double norm = par[0];
    const double gain = par[1];
    const double zero = par[2];
    const double noise = par[3];
    const double avnpe = par[4];
    const double excess = par[5];
    const double xtalk = par[6];
    const double q = (x[0] - zero) / gain;

    double y = 0.;
    for (unsigned p = 0; p <= gFitMaxNpe; ++p) {
        for (unsigned s = 0; s <= gFitMaxNpe; ++s) {
            y += TMath::Poisson(p, avnpe) * TMath::Poisson(s, (p * xtalk)) *
                 TMath::Gaus(q, (p + s), TMath::Sqrt(noise * noise + (p + s) * excess * excess), true);
        }
    }
    y *= norm;

    return y;
}


#endif //WAVETEST_MPPCFIT_H
