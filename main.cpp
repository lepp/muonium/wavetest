#include <algorithm>
#include <array>
#include <cerrno>
#include <chrono>
#include <csignal>
#include <cstdint>
#include <cstdio>
#include <iostream>
#include <numeric>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

#include <argp.h>

#include "rapidjson/document.h"
#include "rapidjson/filereadstream.h"

#include "TApplication.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "THStack.h"
#include "TLine.h"
#include "TStyle.h"
#include "TSystem.h"

#include "git-revision.h"
#include "WDBLib.h"


#define LINECOLOUR 30
#define LINEWIDTH 3
#define N_CHANNELS 16
#define N_SAMPLES 1024


//////////////////////
// Argument parsing //
//////////////////////

const char *argp_program_version = GIT_REVISION;
const char *argp_program_bug_address = "<dg.code+wavetest@protonmail.ch>";

// Program documentation
static char doc[] = "wavetest -- a test DAQ for the WaveDREAM board";

// Description of positional arguments
static char args_doc[] = "WDBCONFIG";

// Optional arguments
static struct argp_option options[] = {
        {"verbosity", 'v', "VERB",   0, "Set verbosity level"},
        {"noreboot",  'r', nullptr,  0, "Do not reboot WDB prior to configuration"},
        {"hvon",      'h', nullptr,  0, "Leave HV on on exit"},
        {"wdsdir",    'w', "WDSDIR", 0, "WDS directory containing calibration data"},
        {"spectrum",  's', "CONFIG", 0, "Run in spectrum mode"},
        {"savewf",    'o', "WFFILE", 0, "Save waveforms to file"},
        {nullptr}
};

// Used by main to communicate with parse_opt
struct arguments {
    std::string wdbConfig;
    int verbosity = 0;
    bool noReboot = false;
    bool hvOn = false;
    std::string wdsDir;
    std::string spectrumConfig;
    std::string wfFile;
};

// Argp parser function
static error_t parse_opt(int key, char *arg, struct argp_state *state) {
    // Get the input argument from argp_parse, which we know is a pointer to our arguments structure
    auto *args = static_cast<arguments *>(state->input);
    switch (key) {
        case 'v':
            args->verbosity = std::stoi(arg);
            break;
        case 'r':
            args->noReboot = true;
            break;
        case 'h':
            args->hvOn = true;
            break;
        case 'w':
            args->wdsDir = arg;
            break;
        case 's':
            args->spectrumConfig = arg;
            break;
        case 'o':
            args->wfFile = arg;
            break;
        case ARGP_KEY_ARG:
            if (state->arg_num >= 1) {
                // Too many arguments
                argp_usage(state);
            }
            args->wdbConfig = arg;
            break;
        case ARGP_KEY_END:
            if (state->arg_num < 1) {
                // Not enough arguments
                argp_usage(state);
            }
            break;
        default:
            return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

// Argp parser
static struct argp parser = {options, parse_opt, args_doc, doc};


/////////////////////
// Signal handling //
/////////////////////

// From cppreference.com
namespace {
    volatile std::sig_atomic_t gSignalStatus;
}

void signalHandler(int signal) {
    gSignalStatus = signal;
}


/////////////
// Globals //
/////////////

enum SPECMODE {
    cSpecNull,
    cSpecPeak,
    cSpecInt
};

typedef struct GLOBALS {
    bool reboot = true;
    bool hvOff = true;
    int verbose = 0;
    std::string logFileName;
    std::string wdsDir;
    bool daqNormal = false;
    int drsChTxEn = 0;
    bool fastReadout = false;
    std::vector<WDB *> wdb;
    WP *wp = nullptr;
    std::uint64_t nEvents = 0;
    TH2D *scalerHist = nullptr;
    std::vector<TH1F> wfHist;
    THStack *wfStack = nullptr;
    TFile *wfFile = nullptr;
    SPECMODE specMode = cSpecNull;
    std::string specFile;
    std::uint64_t specNevents = 0;
    unsigned specBins = 0;
    double specMin = 0.;
    double specMax = 0.;
    unsigned specUpdateInterval = 0;
    std::vector<TH1D> specHist;
    THStack *specStack = nullptr;
    double T1Int = 0.;
    double T2Int = 0.;
    double T1Baseline = 0.;
    double T2Baseline = 0.;
    TLine *line = nullptr;
    std::vector<TCanvas *> canvas;
} GLOBALS;


//////////////////////
// Helper functions //
//////////////////////

// Create integer mask from JSON array of 0s and 1s
// Validity of JSON array needs to be checked beforehand
int jsonArray2intMask(const rapidjson::Value &array) {
    int mask = 0;
    unsigned chn = 0;
    for (const auto &value : array.GetArray()) {
        if (value.IsUint() && (value.GetUint() == 1)) {
            mask |= 1 << chn;
        }
        ++chn;
    }

    return mask;
}

// Create string of 0s and 1s from (unsigned) integer mask
std::string intMask2stringMask(const unsigned mask) {
    std::ostringstream stringMask;
    stringMask << mask << " (";
    for (unsigned chn = 0; chn < N_CHANNELS; ++chn) {
        stringMask << ((mask & (1 << chn)) ? "1" : "0");
    }
    stringMask << ")";

    return stringMask.str();
}

// Function to recreate the canvas if the user closed it
bool cdCanvas(GLOBALS &gl, const unsigned canvas, const int subpad = 0) {
    bool reset = false;
    if (!gl.canvas[canvas] || !gl.canvas[canvas]->GetCanvasImp()) {
        if (gl.canvas[canvas]) {
            delete gl.canvas[canvas];
        }
        gl.canvas[canvas] = new TCanvas;
        reset = true;
    }
    gl.canvas[canvas]->cd(subpad);

    return reset;
}


//////////////////
// JSON parsing //
//////////////////

void parseError(const std::string &entry, const std::string &fileName) {
    throw std::runtime_error("Failed to parse " + entry + " from " + fileName);
}

rapidjson::Document readJson(const std::string &fileName) {
    FILE *jsonFile = std::fopen(fileName.c_str(), "r");
    if (!jsonFile) {
        throw std::runtime_error("Failed to open " + fileName);
    }
    char jsonReadBuffer[65536];
    rapidjson::FileReadStream jsonStream(jsonFile, jsonReadBuffer, sizeof(jsonReadBuffer));
    rapidjson::Document jsonDoc;
    jsonDoc.ParseStream(jsonStream);
    fclose(jsonFile);
    if (jsonDoc.HasParseError() || !jsonDoc.IsObject()) {
        throw std::runtime_error("Failed to parse " + fileName);
    }
    return jsonDoc;
}

void configureSpectrum(GLOBALS &gl, const std::string &configFileName) {
    rapidjson::Document config = readJson(configFileName);
    std::string entry = "SpecMode";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsString()) {
        if (std::string(config[entry.c_str()].GetString()) == "peak") {
            gl.specMode = cSpecPeak;
        } else if (std::string(config[entry.c_str()].GetString()) == "integral") {
            gl.specMode = cSpecInt;
        }
    }
    if (!gl.specMode) {
        parseError(entry, configFileName);
    }
    entry = "SpecFile";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsString()) {
        gl.specFile = config[entry.c_str()].GetString();
    } else {
        parseError(entry, configFileName);
    }
    entry = "SpecNevents";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsUint64()) {
        gl.specNevents = config[entry.c_str()].GetUint64();
    } else {
        parseError(entry, configFileName);
    }
    entry = "SpecBins";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsUint()) {
        gl.specBins = config[entry.c_str()].GetUint();
    } else {
        parseError(entry, configFileName);
    }
    entry = "SpecMin";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsDouble()) {
        gl.specMin = config[entry.c_str()].GetDouble();
    } else {
        parseError(entry, configFileName);
    }
    entry = "SpecMax";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsDouble()) {
        gl.specMax = config[entry.c_str()].GetDouble();
    } else {
        parseError(entry, configFileName);
    }
    entry = "SpecUpdateInterval";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsUint()) {
        gl.specUpdateInterval = config[entry.c_str()].GetUint();
    } else {
        parseError(entry, configFileName);
    }
    entry = "T1Int";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsDouble()) {
        gl.T1Int = config[entry.c_str()].GetDouble();
    } else {
        parseError(entry, configFileName);
    }
    entry = "T2Int";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsDouble()) {
        gl.T2Int = config[entry.c_str()].GetDouble();
    } else {
        parseError(entry, configFileName);
    }
    entry = "T1Baseline";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsDouble()) {
        gl.T1Baseline = config[entry.c_str()].GetDouble();
    } else {
        parseError(entry, configFileName);
    }
    entry = "T2Baseline";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsDouble()) {
        gl.T2Baseline = config[entry.c_str()].GetDouble();
    } else {
        parseError(entry, configFileName);
    }
    entry = "FastReadout";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsBool()) {
        gl.fastReadout = config[entry.c_str()].GetBool();
    } else {
        parseError(entry, configFileName);
    }
}

void configureWDB(GLOBALS &gl, WDB *b, const rapidjson::Document &config) {
    std::string entry = "DrsSampleFreq";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsUint()) {
        b->SetDrsSampleFreq(config[entry.c_str()].GetUint());
        const unsigned freq = b->GetDrsSampleFreqMhz();
        // load calibration data for board
        b->LoadVoltageCalibration(freq, gl.wdsDir);
        b->LoadTimeCalibration(freq, gl.wdsDir);
        if (gl.verbose) {
            std::cout << "Set " << entry << " to " << freq << std::endl;
        }
    }
    entry = "HVTarget";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsArray()) {
        int chn = 0;
        for (const auto &value : config[entry.c_str()].GetArray()) {
            if (value.IsFloat()) {
                b->SetHVTarget(chn, value.GetFloat());
            }
            ++chn;
        }
        std::vector<float> HVTarget;
        b->GetHVTarget(HVTarget);
        std::cout << "Setting " << entry << " to:\n";
        for (const auto &v : HVTarget) {
            std::cout << v << ",";
        }
        std::cout << "\b " << std::endl;
        const unsigned waits = (gl.reboot ? 20 : 5);
        std::cout << "Waiting " << waits << "s for currents [uA] to stabilise..." << std::endl;
        for (unsigned t = 0; t < waits; ++t) {
            sleep_ms(1000);
            std::vector<float> HVCurrents;
            b->GetHVCurrents(HVCurrents, true);
            for (const auto &i : HVCurrents) {
                std::cout << i << ",";
            }
            std::cout << "\b " << std::endl;
        }
        HVTarget.clear();
        b->GetHVTarget(HVTarget);
        std::cout << "Set " << entry << " to:\n";
        for (const auto &v : HVTarget) {
            std::cout << v << ",";
        }
        std::cout << "\b " << std::endl;
    }
    entry = "FeGain";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsArray()) {
        int chn = 0;
        for (const auto &value : config[entry.c_str()].GetArray()) {
            if (value.IsFloat()) {
                b->SetFeGain(chn, value.GetFloat());
                if (gl.verbose) {
                    std::cout << "Set " << entry << " for channel " << chn << " to " << b->GetFeGain(chn) << std::endl;
                }
            }
            ++chn;
        }
    }
    entry = "Range";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsFloat()) {
        b->SetRange(config[entry.c_str()].GetFloat());
        if (gl.verbose) {
            std::cout << "Set " << entry << " to " << b->GetRange() << std::endl;
        }
    }
    entry = "ChnTxEn";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsArray()) {
        b->SetChnTxEn(jsonArray2intMask(config[entry.c_str()]));
        if (gl.verbose) {
            std::cout << "Set " << entry << " to " << intMask2stringMask(b->GetChnTxEn()) << std::endl;
        }
    }
    entry = "DrsChTxEn";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsArray()) {
        gl.drsChTxEn = jsonArray2intMask(config[entry.c_str()]);
        b->SetDrsChTxEn(gl.drsChTxEn);
        if (gl.verbose) {
            std::cout << "Set " << entry << " to " << intMask2stringMask(b->GetDrsChTxEn()) << std::endl;
        }
    }
    entry = "ExtAsyncTriggerEn";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsUint()) {
        b->SetExtAsyncTriggerEn(config[entry.c_str()].GetUint());
        if (gl.verbose) {
            std::cout << "Set " << entry << " to " << b->GetExtAsyncTriggerEn() << std::endl;
        }
    }
    entry = "PatternTriggerEn";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsUint()) {
        b->SetPatternTriggerEn(config[entry.c_str()].GetUint());
        if (gl.verbose) {
            std::cout << "Set " << entry << " to " << b->GetPatternTriggerEn() << std::endl;
        }
    }
    entry = "DacTriggerLevelV";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsArray()) {
        int chn = 0;
        for (const auto &value : config[entry.c_str()].GetArray()) {
            if (value.IsFloat()) {
                b->SetDacTriggerLevelV(chn, value.GetFloat());
                if (gl.verbose) {
                    std::cout << "Set " << entry << " for channel " << chn << " to " << b->GetDacTriggerLevelV(chn)
                              << std::endl;
                }
            }
            ++chn;
        }
    }
    entry = "TrgPtrnEn";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsArray()) {
        b->SetTrgPtrnEn(jsonArray2intMask(config[entry.c_str()]));
        if (gl.verbose) {
            std::cout << "Set " << entry << " to " << intMask2stringMask(b->GetTrgPtrnEn()) << std::endl;
        }
    }
    entry = "TrgSrcPolarity";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsArray()) {
        b->SetTrgSrcPolarity(jsonArray2intMask(config[entry.c_str()]));
        if (gl.verbose) {
            std::cout << "Set " << entry << " to " << intMask2stringMask(b->GetTrgSrcPolarity()) << std::endl;
        }
    }
    entry = "TrgSrcEnPtrn";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsArray()) {
        int idx = 0;
        for (const auto &value : config[entry.c_str()].GetArray()) {
            if (value.IsArray()) {
                b->SetTrgSrcEnPtrn(idx, jsonArray2intMask(value));
                if (gl.verbose) {
                    std::cout << "Set " << entry << idx << " to " << intMask2stringMask(b->GetTrgSrcEnPtrn(idx))
                              << std::endl;
                }
            }
            ++idx;
        }
    }
    entry = "TrgStatePtrn";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsArray()) {
        int idx = 0;
        for (const auto &value : config[entry.c_str()].GetArray()) {
            if (value.IsArray()) {
                b->SetTrgStatePtrn(idx, jsonArray2intMask(value));
                if (gl.verbose) {
                    std::cout << "Set " << entry << idx << " to " << intMask2stringMask(b->GetTrgStatePtrn(idx))
                              << std::endl;
                }
            }
            ++idx;
        }
    }
    entry = "TriggerDelayNs";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsUint()) {
        b->SetTriggerDelayNs(config[entry.c_str()].GetUint());
        if (gl.verbose) {
            std::cout << "Set " << entry << " to " << b->GetTriggerDelayNs() << std::endl;
        }
    }
    entry = "TriggerHoldoff";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsInt()) {
        b->SetTriggerHoldoff(config[entry.c_str()].GetInt());
        if (gl.verbose) {
            std::cout << "Set " << entry << " to " << b->GetTriggerHoldoff() << std::endl;
        }
    }
    entry = "ZeroSuprEn";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsBool()) {
        b->SetZeroSuprEn(config[entry.c_str()].GetBool());
        if (gl.verbose) {
            std::cout << "Set " << entry << " to " << b->GetZeroSuprEn() << std::endl;
        }
    }
    entry = "SclTxEn";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsUint()) {
        b->SetSclTxEn(config[entry.c_str()].GetUint());
        if (gl.verbose) {
            std::cout << "Set " << entry << " to " << b->GetSclTxEn() << std::endl;
        }
    }
    entry = "DaqNormal";
    if (config.HasMember(entry.c_str()) && config[entry.c_str()].IsBool()) {
        b->SetDaqNormal(config[entry.c_str()].GetBool());
        gl.daqNormal = config[entry.c_str()].GetBool();
        if (gl.verbose) {
            std::cout << "Set " << entry << " to " << b->GetDaqNormal() << std::endl;
        }
    }
}


///////////////////////////
// WDB utility functions //
///////////////////////////

void rebootWDB(const GLOBALS &gl, WDB *b) {
    if (gl.verbose) {
        std::cout << "Reboot " << b->GetAddr() << std::endl;
    }
    b->ReconfigureFpga();
    sleep_ms(5000);
    if (gl.verbose) {
        std::cout << "Finished" << std::endl;
    }
}

void connectWDB(const GLOBALS &gl, WDB *b) {
    b->SetVerbose(gl.verbose);
    b->SetLogFile(gl.logFileName);
    b->Connect();
    if (gl.reboot) {
        rebootWDB(gl, b);
        b->Connect();
    }

    for (int i = 0; i < 50; ++i) {
        b->ReceiveStatusRegisters();
        int s = b->GetBoardMagic();
        if (s != 0xAC) {
            sleep_ms(100);
            std::cout << "Wait for board magic number" << std::endl;
        } else {
            break;
        }
        if (i == 49) {
            throw std::runtime_error(std::string("Error reading magic number from " + b->GetName()));
        }
    }

    b->ReceiveControlRegisters();
    if (gl.verbose) {
        std::cout << "\n========== WDB Info ==========" << std::endl;
        b->PrintVersion();
    }

    // Disable normal HW trigger to prevent network congestion
    b->SetDaqNormal(false);

    b->SetDestinationPort(gl.wp->GetServerPort());
    // Enable ethernet and disable serdes as we're not in a crate
    b->SetEthComEn(1);
    b->SetSerdesComEn(0);
}

void disconnectWDB(GLOBALS &gl, WDB *b) {
    if (gl.hvOff) {
        std::cout << "Turn off HV" << std::endl;
        for (int chn = 0; chn < N_CHANNELS; ++chn) {
            b->SetHVTarget(chn, 0.);
        }
    } else {
        std::cout << "Not turning off HV as per user request" << std::endl;
    }
    if (gl.verbose) {
        std::cout << "Disconnect from " << b->GetAddr() << std::endl;
    }
    // Make sure normal HW trigger is disabled
    b->SetDaqNormal(false);
    // Wait for WP to finish receiving events, otherwise it crashes
    sleep_ms(1000);
    // Now disconnect for real
    gl.wdb.erase(std::remove(gl.wdb.begin(), gl.wdb.end(), b), gl.wdb.end());
    delete b;
}


/////////////////////////
// Waveform processing //
/////////////////////////

typedef struct wfSorted {
    std::array<double, (N_SAMPLES + 1)> t{};
    std::array<float, N_SAMPLES> u{};
} wfSorted;

// Due to differences in PCB trace lengths, calibrated waveform sample times can appear out of order.
// So we need to sort them first.
// TH1F expects bin edges to be double, so we need to convert the sample times accordingly.
wfSorted sortWaveform(const GLOBALS &gl, const WDEvent &event, const int chn) {
    wfSorted wf;
    const float *tUnsorted = event.mWfTDRS[chn];
    std::vector<std::size_t> idx(N_SAMPLES);
    std::iota(idx.begin(), idx.end(), 0);
    std::sort(idx.begin(), idx.end(),
              [tUnsorted](const std::size_t a, const std::size_t b) { return tUnsorted[a] < tUnsorted[b]; });
    for (std::size_t s = 0; s < N_SAMPLES; ++s) {
        wf.t[s] = event.mWfTDRS[chn][idx[s]];
        wf.u[s] = event.mWfUDRS[chn][idx[s]];
    }
    wf.t[N_SAMPLES] = 2 * wf.t[N_SAMPLES - 1] - wf.t[N_SAMPLES - 2];

    return wf;
}

double calculatePulseHeight(const WDEvent &event, const int chn) {
    return -*std::min_element(std::begin(event.mWfUDRS[chn]), std::end(event.mWfUDRS[chn]));
}

double calculateCharge(const GLOBALS &gl, const wfSorted &wf) {
    const std::array<double, (N_SAMPLES + 1)> &t = wf.t;
    const std::array<float, N_SAMPLES> &u = wf.u;
    double baseline = 0.;
    std::size_t nBaseline = 0;
    double charge = 0.;

    for (std::size_t idx = 0; idx < N_SAMPLES; ++idx) {
        if ((t[idx] >= gl.T1Baseline) && (t[idx] <= gl.T2Baseline)) {
            baseline += u[idx];
            ++nBaseline;
        }
    }
    if (nBaseline > 0) {
        baseline /= static_cast<double>(nBaseline);
    }

    for (std::size_t idx = 0; idx < (N_SAMPLES - 1); ++idx) {
        if ((t[idx + 1] > gl.T1Int) && (t[idx] < gl.T2Int)) {
            double t1;
            double t2;
            double u1;
            double u2;
            if (t[idx] < gl.T1Int) {
                t1 = gl.T1Int;
                u1 = u[idx] + (u[idx + 1] - u[idx]) * (gl.T1Int - t[idx]) / (t[idx + 1] - t[idx]);
            } else {
                t1 = t[idx];
                u1 = u[idx];
            }
            if (t[idx + 1] > gl.T2Int) {
                t2 = gl.T2Int;
                u2 = u[idx] + (u[idx + 1] - u[idx]) * (gl.T2Int - t[idx]) / (t[idx + 1] - t[idx]);
            } else {
                t2 = t[idx + 1];
                u2 = u[idx + 1];
            }
            charge += .5 * (u1 + u2 - 2 * baseline) * (t2 - t1);
        }
    }
    // pC into 50Ohm
    charge *= -1e12 / 50.;

    return charge;
}

void drawWf(GLOBALS &gl) {
    cdCanvas(gl, 1);
    gl.wfStack->Draw("nostack");
    // Axes only exist after drawing (see ROOT docs)
    gl.wfStack->GetXaxis()->SetTitle("Time [s]");
    gl.wfStack->GetYaxis()->SetTitle("Voltage [V]");
    gPad->Modified();
    // BuildLegend doesn't work after gPad->Update()
    gPad->BuildLegend(.8, .1, .9, .7);
    gPad->Update();
}

void drawSpectrum(GLOBALS &gl) {
    cdCanvas(gl, 2);
    gl.specStack->Draw("nostack");
    // Axes only exist after drawing (see ROOT docs)
    if (gl.specMode == cSpecPeak) {
        gl.specStack->GetXaxis()->SetTitle("Pulse Height [V]");
    } else if (gl.specMode == cSpecInt) {
        gl.specStack->GetXaxis()->SetTitle("Charge [pC]");
    }
    gPad->Modified();
    // BuildLegend doesn't work after gPad->Update()
    gPad->BuildLegend(.8, .3, .9, .9);
    gPad->Update();
}

void processWaveform(GLOBALS &gl, const WDEvent &event) {
    if (gl.verbose) {
        std::cout << "\nProcessing event number " << event.mEventNumber << "...\n" << std::endl;
    }
    bool firstWf = true;
    bool firstSpec = true;
    for (int c = 0; c < N_CHANNELS; ++c) {
        if (event.mDRSChannelPresent[c]) {
            const wfSorted wf = sortWaveform(gl, event, c);
            TH1F &wfHist = gl.wfHist[c];
            wfHist.Reset();
            wfHist.SetBins(N_SAMPLES, wf.t.data());
            wfHist.Set(N_SAMPLES, wf.u.data());
            if (gl.wfFile) {
                wfHist.Write(("event" + std::to_string(event.mEventNumber) + "_ch" + std::to_string(c)).c_str());
            }
            gl.wfStack->SetTitle(("Event " + std::to_string(event.mEventNumber)).c_str());
            if (gl.specMode) {
                if (gl.specMode == cSpecPeak) {
                    gl.specHist[c].Fill(calculatePulseHeight(event, c));
                } else if (gl.specMode == cSpecInt) {
                    gl.specHist[c].Fill(calculateCharge(gl, wf));
                }
            }
        }
    }
    drawWf(gl);
    if (gl.specMode == cSpecInt) {
        gl.line->SetLineColor(kBlue);
        gl.line->DrawLine(gl.T1Baseline, 0., gl.T2Baseline, 0.);
        gl.line->SetLineColor(kRed);
        gl.line->DrawLine(gl.T1Int, 0., gl.T2Int, 0.);
    }
    gPad->Modified();
    gPad->Update();
    if (gl.specMode) {
        drawSpectrum(gl);
    }
    ++gl.nEvents;
}

void processWaveformFast(GLOBALS &gl, const WDEvent &event) {
    for (int c = 0; c < N_CHANNELS; ++c) {
        if (event.mDRSChannelPresent[c]) {
            if (gl.specMode == cSpecPeak) {
                gl.specHist[c].Fill(calculatePulseHeight(event, c));
            } else if (gl.specMode == cSpecInt) {
                gl.specHist[c].Fill(calculateCharge(gl, sortWaveform(gl, event, c)));
            }
        }
    }
    ++gl.nEvents;
    if (!(gl.nEvents % gl.specUpdateInterval)) {
        drawSpectrum(gl);
        if (gl.verbose) {
            std::cout << "Processed " << gl.nEvents << " events so far" << std::endl;
        }
    }
}

void processEvent(GLOBALS &gl, WDB *b) {
    WDEvent event(b->GetSerialNumber());
    gl.wp->SetRequestedBoard(b);
    if (!gl.daqNormal) {
        sleep_ms(b->GetTriggerHoldoff());
        b->SetDaqSingle(1);
    }

    // read waveforms
    // increase timeout for slow XML logging
    const bool bNewEvent = gl.wp->GetLastEvent(b, (gl.wp->IsXMLLogging() ? 5000 : 500), event);
    if (bNewEvent && event.mHasDRSData) {
        if (gl.fastReadout) {
            processWaveformFast(gl, event);
        } else {
            processWaveform(gl, event);
        }
    }
    if (gl.specMode && (gl.nEvents >= gl.specNevents)) {
        signalHandler(SIGINT);
    }
}

void processScalers(GLOBALS &gl, WDB *b) {
    std::vector<std::uint64_t> scaler;
    b->GetScalers(scaler, true);
    if (gl.verbose) {
        std::cout << "\n========== HW scalers [Hz] ==========\n";
        std::string chn;
        for (std::size_t i = 0; i < WD_N_SCALER; ++i) {
            if (i < N_CHANNELS) {
                chn = std::to_string(i);
            } else if (i == N_CHANNELS) {
                chn = "T";
            } else if (i == N_CHANNELS + 1) {
                chn = "E";
            } else if (i == N_CHANNELS + 2) {
                chn = "C";
            }
            std::cout << chn << ":\t" << scaler[i] << "\n";
        }
        std::cout << std::endl;
    }
    gl.scalerHist->Reset();
    for (int col = 0, chn = 0; col < 5; ++col) {
        for (int row = 0; row < 4; ++row, ++chn) {
            gl.scalerHist->SetBinContent((col + 1), (row + 1), ((chn < WD_N_SCALER) ? scaler[chn] : 0.));
        }
    }
    cdCanvas(gl, 0);
    gl.scalerHist->Draw("colz");
    gPad->Modified();
    gPad->Update();
}


//////////
// main //
//////////

int main(int argc, char *argv[]) {
    struct arguments args;
    argp_parse(&parser, argc, argv, 0, nullptr, &args);

    rapidjson::Document wdbConfig = readJson(args.wdbConfig);

    GLOBALS gl;
    gl.reboot = !args.noReboot;
    gl.hvOff = !args.hvOn;
    gl.verbose = args.verbosity;
    gl.wdsDir = args.wdsDir;
    if (!args.spectrumConfig.empty()) {
        configureSpectrum(gl, args.spectrumConfig);
    }
    if (!args.wfFile.empty()) {
        gl.wfFile = new TFile(args.wfFile.c_str(), "RECREATE");
        if (gl.wfFile->IsZombie()) {
            throw std::runtime_error("Failed to open " + args.wfFile);
        }
    }
    std::string cfgEntry = "LogFileName";
    if (wdbConfig.HasMember(cfgEntry.c_str()) && wdbConfig[cfgEntry.c_str()].IsString()) {
        gl.logFileName = wdbConfig[cfgEntry.c_str()].GetString();
        if (gl.verbose) {
            std::cout << "Logging to " << gl.logFileName << std::endl;
        }
    }
    gl.canvas = std::vector<TCanvas *>(3, nullptr);
    gl.wfStack = new THStack("wfStack", "Waveform");
    gl.wfHist.reserve(N_CHANNELS);
    for (std::size_t chn = 0; chn < N_CHANNELS; ++chn) {
        gl.wfHist.emplace_back(("wfHist" + std::to_string(chn)).c_str(), ("Ch" + std::to_string(chn)).c_str(),
                               N_SAMPLES, 0, N_SAMPLES);
        gl.wfStack->Add(&gl.wfHist[chn], "hist");
        gl.wfHist[chn].SetLineColor(LINECOLOUR + chn);
        gl.wfHist[chn].SetLineWidth(LINEWIDTH);
    }
    if (gl.specMode) {
        if (gl.specMode == cSpecPeak) {
            gl.specStack = new THStack("specStack", "Pulse Height Spectrum");
        } else if (gl.specMode == cSpecInt) {
            gl.specStack = new THStack("specStack", "Charge Spectrum");
        }
        gl.specHist.reserve(N_CHANNELS);
        for (std::size_t chn = 0; chn < N_CHANNELS; ++chn) {
            gl.specHist.emplace_back(("specHist" + std::to_string(chn)).c_str(), ("Ch" + std::to_string(chn)).c_str(),
                                     gl.specBins, gl.specMin, gl.specMax);
            gl.specStack->Add(&gl.specHist[chn], "hist");
            gl.specHist[chn].SetLineColor(LINECOLOUR + chn);
            gl.specHist[chn].SetLineWidth(LINEWIDTH);
        }
    }
    TH2D scalerHist("scalerHist", "HW Scalers [Hz]", 5, 0, 5, 4, 0, 4);
    gl.scalerHist = &scalerHist;
    TLine line;
    line.SetLineWidth(2 * LINEWIDTH);
    gl.line = &line;
    gStyle->SetPalette(kCool);
    gStyle->SetOptStat(0);

    cfgEntry = "Address";
    if (!(wdbConfig.HasMember(cfgEntry.c_str()) && wdbConfig[cfgEntry.c_str()].IsString())) {
        parseError(cfgEntry, args.wdbConfig);
    }
    // Need to dynamically allocate the waveform processor.
    // Otherwise it generates a SIGABRT at the end.
    // The same happens when manually deleting it.
    // Don't yet understand the reason fully.
    gl.wp = new WP(gl.verbose, gl.wdsDir, gl.logFileName);
    WDB *wdb = new WDB(wdbConfig[cfgEntry.c_str()].GetString(), gl.verbose);
    connectWDB(gl, wdb);
    configureWDB(gl, wdb, wdbConfig);
    gl.wdb.push_back(wdb);
    gl.wp->SetWDBList(gl.wdb);

    // Only pass the program name to ROOT
    int rootArgc = 1;
    TApplication app("app", &rootArgc, argv);
    std::signal(SIGINT, signalHandler);

    const auto clkStart = std::chrono::steady_clock::now();
    while (!gSignalStatus) {
        if (!gl.fastReadout) {
            processScalers(gl, wdb);
        }
        processEvent(gl, wdb);
        gSystem->ProcessEvents();
    }
    const auto clkStop = std::chrono::steady_clock::now();
    const std::chrono::duration<double> clkDuration = clkStop - clkStart;
    std::cout << "\nProcessed " << gl.nEvents << " events in " << clkDuration.count() << "s at a rate of "
              << (static_cast<double>(gl.nEvents) / clkDuration.count()) << "Hz\n" << std::endl;

    disconnectWDB(gl, wdb);

    if (gl.wfFile) {
        gl.wfFile->Close();
        delete gl.wfFile;
    }

    if (gl.specMode) {
        if (gl.verbose) {
            std::cout << "Writing spectrum to " << gl.specFile << "..." << std::endl;
        }
        gl.canvas[2]->SaveAs((gl.specFile + ".pdf").c_str());
        TFile specFile((gl.specFile + ".root").c_str(), "RECREATE");
        if (!specFile.IsZombie()) {
            for (const auto &hist : gl.specHist) {
                hist.Write();
            }
            specFile.Close();
            std::cout << "Done" << std::endl;
        } else {
            // Don't die, just print a warning. We're at the end anyway, and might need to turn off HV.
            std::cerr << "Warning: Failed to open " << gl.specFile << ". Not saving Spectrum." << std::endl;
        }
        delete gl.specStack;
    }
    delete gl.wfStack;

    std::cout << "\nBye!" << std::endl;
    return 0;
}
